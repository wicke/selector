<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">  
    <title>Northumberland Site Selector</title>
    <link rel="stylesheet" type="text/css" href="../public/css/report.css">
    <style>
         @page {margin:0;}
        body {font-family: 'Helvetica';}
        .wrapper { margin: 5px auto; width: 100%;}
        .header {padding: 0 10px 5px;margin: 0 0 5px; border-bottom: 2px solid #555555;}
        .top > h2 {margin: 10px 5px; color: #555555;}
        .top {height: 40px; border-top: 2px solid #555555;}
        table { font-size: 10px;}
        table tr td { padding: 10px;}
        table tr th { background: #cccccc;}    
        table tr:nth-child(odd) { background: #efefef; }      
        .page-break {  page-break-after: always; }
    </style>
</head>
<body>    
    <div class="wrapper">  
    <div class="top">
        <h2 style="text-align: center;">Town of Cobourg Inventory Of Available Land And Buildings</h2>
    </div>
    <div class="header">
        <p style="text-align: center;font-size: 0.9em;">
            For details on any of these properties contact the Business and Tourism Centre<br>
            212 King Street West &bull; Cobourg, Ontario K9A 2N1<br>
            Phone 905-372-5481 &bull; Toll Free: 1-888-262-6874
        </p>    
        <p style="font-size: 0.7em; text-align: center;">DISCLAIMER: This information has been obtained from sources believed reliable. Information for this database is supplied solely by The Town of Cobourg landowners, realtor and other third party sources. The Town of Cobourg Economic Development office makes no guarantee, warranty or representation that the information above is accurate. Potential investors are advised to conduct a careful, independent investigation of the property in order to determine whether the property can satisfy their investment needs. All figures are in Canadian dollars. Please note space measurements may be approximate.</p>    
    </div>
    <div class="body">
        @foreach($categories as $category)                                
            @foreach($subcategories as $subcategory)
                <h1 style="padding: 0 0 0 10px; background:#555555; color:#ffffff;">{!! $category->category !!} {!! $subcategory->subcategory !!} </h1>                               
                 <table page-break-inside: auto;>
                    <thead>
                        <tr>
                            <th style="width: 5%;">Record</th>
                            <th style="width: 5%;">Zoning</th>
                            <th style="width: 15%;">Address</th>                                        
                            <th style="width: 5%;">Sale or Lease</th>
                            <th style="width: 5%;">Category</th>
                            <th style="width: 40%;">Description</th>                       
                            <th>Contact</th>  
                        </tr>
                    </thead>                         
                @foreach($sites as $site)                    
                    @if($site->category_id == $category->id && $site->subcategory_id == $subcategory->id)
                        <tr>    
                            <td>{!! $site->record !!}</td>
                            <td>{!! $site->zoning->zoning_code !!}</td>
                            <td>{!! $site->address !!} {!! $site->town !!}</td>   
                            <td>
                                {!! ($site->for_sale == 1 && $site->for_lease == 0 ? 'SALE' : ''  ) !!}
                                {!! ($site->for_lease == 1 && $site->for_sale == 0 ? ' LEASE' : ''  ) !!}
                                {!! ($site->for_lease == 1 && $site->for_sale == 1  ? ' SALE or LEASE' : ''  ) !!}
                            </td>
                            <td>{!! $site->category->category !!} {!! $site->subcategory->subcategory !!}</td>
                            <td>{!! $site->description !!}</td>                           
                            <td>
                                @foreach($site->contact as $p)                            
                                    {!! $p->first_name !!} {!! $p->last_name !!}<br />
                                    {!! $p->company !!}<br />
                                    {!! (strlen($p->phone) == 10 ? (substr($p->phone, 0, 3) . '-' . substr($p->phone, 3, 3) . '-' . substr($p->phone, 6, 4)) : $p->phone ) !!}  <br><br>                         
                                @endforeach
                            </td>
                        </tr>                        
                    @endif
                @endforeach
                </table>
                <div class="page-break"></div>     
            @endforeach       
        @endforeach
    </div>

    <div class="body">
      
    </div>

    <div class="footer">
       <p style="font-size: 0.8em; text-align: center;">DISCLAIMER: This information has been obtained from sources believed reliable. Information for this database is supplied solely by The Town of Cobourg landowners, realtor and other third party sources. The Town of Cobourg Economic Development office makes no guarantee, warranty or representation that the information above is accurate. Potential investors are advised to conduct a careful, independent investigation of the property in order to determine whether the property can satisfy their investment needs. All figures are in Canadian dollars. Please note space measurements may be approximate.</p>
    </div>
   </div>
</body>

</html>