@extends('layouts.report')

@section('address')
    <h2 style="padding: 0 0 0 10px;  color:#555555;">{!! $site->address !!} - {!! $site->town !!}</h2>
@stop

@section('content')
  <div style="float:left; width: 55%;margin: 20px 10px;">
    <table style="font-size: 14px;">       
      <tbody>
        <tr>    
        <td></td>      
          <td><strong>{!! $site->category->category !!} {!! $site->subcategory->subcategory !!}</strong></td>
        </tr>
        <tr>
          <td style="width: 75px;">Address:</td>
          <td>{!! $site->address !!}, {!! $site->town !!} ON</td>
        </tr>        
        <tr>
           <td>Sq Ft:</td>
           <td>{!! $site->total_sq_ft !!}</td>
        </tr>
        <tr>
          <td>Acres: </td>
          <td>{!! $site->acres !!}</td>
        </tr>
        <tr>
          <td>Zoning: </td>
          <td>{!! $site->zoning->zoning_code !!} : {!! $site->zoning->zoning_description !!}</td>
        </tr>
        <tr>
          <td>Service:</td>
          <td>{!! $site->service->service_code !!} : {!! $site->service->service_description !!}</td>
        </tr>
        <tr>
          <td>MLS</td>
          <td>{!! $site->mls  !!} </td>
        </tr>
      </tbody>       
    </table>  
  </div>

<div style="float:left; width: 35%;">    
  @if( file_exists(public_path('uploads/' . $site->id  . '/' .  $site->pic) )  )
     <img src="./uploads/{!! $site->id !!}/{!! $site->pic !!}" style="width: 100%;margin: 20px;"  /> 
  @else
   <img src="./uploads/nopic.jpg" style="width: 100%;margin: 20px;"  />
 @endif
</div>

  <div style="clear: both;margin: 10px 10px;">
    <p>{!! $site->description !!}</p>
  </div>

  <div style="clear: both;margin: 10px 10px;">
    <h3>Contact</h3>
    @foreach($site->contact as $contact)
    <table style="font-size: 14px; padding-bottom: 20px;">  
     <tbody>       
        <tr>
          <td style="width: 75px;">Contact:</td>
          <td>{!! $contact->first_name !!} {!! $contact->last_name !!}</td>
        </tr>
        <tr>
          <td style="width: 75px;">Company:</td>
            <td>{!! $contact->company !!}</td>
        </tr>
         <tr>
          <td style="width: 75px;">Address:</td>
          <td>{!! $contact->address !!}</td>
        </tr>
        <tr>
          <td style="width: 75px;">City:</td>
          <td>{!! $contact->city !!}</td>
        </tr>
         <tr>
          <td style="width: 75px;">Phone:</td>
          <td>{!! (strlen($contact->phone) == 10 ? (substr($contact->phone, 0, 3) . '-' . substr($contact->phone, 3, 3) . '-' . substr($contact->phone, 6, 4)) : $contact->phone ) !!}</td>
        </tr>
         <tr>
          <td style="width: 75px;">Email:</td>
          <td>{!! $contact->email !!}</td>
        </tr>
      </tbody>
    </table>
    @endforeach
  </div>
      
@stop
 
@section('footer')
  <p style="font-size:0.8em;">DISCLAIMER: This information has been obtained from sources believed reliable. Information for this database is supplied solely by The Town of Cobourg landowners, realtor and other third party sources. The Town of Cobourg Economic Development office makes no guarantee, warranty or representation that the information above is accurate. Potential investors are advised to conduct a careful, independent investigation of the property in order to determine whether the property can satisfy their investment needs. All figures are in Canadian dollars.</p>
  <p style="font-size:0.8em;">&copy; {!! date('Y') !!}</p>  
 @stop