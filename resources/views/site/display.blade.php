@extends('layouts.masterB')

@section('content')

<div class="row no-margin">
    <div class="col-sm-1 no-pad">	        
        <div class="photo-nav no-padding">
            <div class="gallery-nav">
                 @if($url)
                    <a href="{!! URL::previous() !!}" class="gallery-prev " title="BACK"><i class="fa fa-chevron-left"></i></a>
                 @endif
                <a href="/site/{!! $site->id !!}/print" class="gallery-next " title="PRINT"><i class="fa fa-print"></i></a>                
            </div>            
        </div>
    </div>
    <div class="col-sm-11 no-pad">
        @if($site)	
            <div class="content-box">		
                <div class="row no-margin">                
                    <div class="col-sm-7">
                        <div class="site-box">
                            <h2 class="list-group-item-heading">{!! $site->address !!} - Cobourg ON</h2>          
                            <table class="table">                                       
                                <tbody>
                                    <tr>
                                        <td>Category</td>
                                        <td>{!! $site->category->category !!} {!! $site->subcategory->subcategory !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>{!!  $site->address !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Zoning</td>
                                        <td>{!!  $site->zoning->zoning_description !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Service</td>
                                        <td>{!!  $site->service->service_description !!} </td>
                                    </tr>
                                    <tr>
                                        <td>MLS</td>
                                        <td>{!!  $site->mls !!} </td>
                                    </tr>
                                    <tr>
                                        <td>Purchase Price</td>
                                        <td>
                                            {!!  $site->purchase_price !!} <br>
                                            {!! $site->purchase_price_notes !!} 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lease Price</td>
                                        <td>
                                            {!!  $site->lease_price !!} <br>
                                            {!! $site->lease_price_notes !!} 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="site-box">       
                            <h2>Description</h2>
                            <p style="font-size: 16px;padding: 10px 10px;">{!! $site->description  !!}</p>    
                        </div>      
                        <div class="site-box">
                            <h2>Contact(s)</h2>                           
                                @foreach($site->contact as $p)                                
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td>Contact</td>
                                            <td>{!! $p->first_name !!} {!! $p->last_name !!}</td>
                                        </tr>                                        
                                        <tr>
                                            <td>Company</td>
                                            <td>{!! $p->company !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Address</td>
                                            <td> {!! $p->address !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td> {!! $p->phone !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td> {!! $p->email !!}</td>
                                        </tr>
                                    </tbody>
                                    </table>                                                                                                                
                                @endforeach                            
                        </div>                        
                    </div>			        
                    <div class="col-sm-5 ">
                        <div style="margin: 0 0 20px; border: 1px solid #bbb;">                          
                          <div style="height: 400px;">{!! $map !!}</div>
                        </div>
                        
                        <div style="margin: 0 0 20px; border: 1px solid #bbb;">
                            @if( file_exists(public_path('uploads/' . $site->id  . '/' .  $site->pic) )  )                            
                                <img class="media-object" src="../uploads/{!! $site->id !!}/{!! $site->pic !!}" alt="#" style="width: 475px;" >                                                             
                            @else
                                <img src="../uploads/nopic.jpg" style="width: 50%;margin: 20px;"  />
                            @endif                                   				                                            
                        </div>
                </div>
            </div>
        @else
	         <p>NO SITE FOUND</p>
        @endif
    </div>
</div>

@stop

@section('map_script')
       
@stop