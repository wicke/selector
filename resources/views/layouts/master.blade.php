<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Northumberland Site Selector APP</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans%7COxygen%7CRoboto+Slab:700%7COswald' rel='stylesheet' type='text/css'>	
</head>
<body>

<div class="wrapper ">
	
<header class="header">
	<div class="sidenav-toggle">
		<div class="fa fa-bars burger"> </div>
	</div>
	<span class="header-title">Northumberland Site Selector</span>
</header>

<aside>
	<div class="sidenav" >


		<ul class="sidenav-list">
			<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="/selector"><i class="fa fa-search"></i> Selector</a></li>

			<li><a href="#"> <i class="fa fa-cog"></i> Settings</a></li>
		</ul>


	</div>
</aside>

<section id="main-container">
	<div class="container-wrapper">
		@yield('content')
	</div>
</section>

<footer>
	<div class="footer-left">&copy; {!! date('Y') !!}</div>
</footer>


</div>

	









	{!! Html::style('/css/master.css') !!}
	{!! Html::style('/bower_components/font-awesome/css/font-awesome.css') !!}
	{!! Html::style('/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('/bower_components/typicons/src/font/typicons.min.css') !!}
	{!! Html::script('/bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('/js/master.js') !!}
	{!! Html::script('/js/selector.js') !!}


</body>
</html>