<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>REPORT</title>
    <style type="text/css">
        @page {margin:0;}
        body {font-family: 'Helvetica';}
        .header {padding: 0 10px 5px;margin: 0 0 5px; border-bottom: 2px solid #555555;}
        .red-header {color:red; font-weight: bold;}
        .float-left {float:left; width: 200px; background: green;}
        .float-right {float:right;width: 300px; background: red;}
        img {border-radius: 5px; border:1px solid #bbbbbb;}
        .wrapper { margin: 5px auto; width: 100%;}
        .footer {padding: 20px;position: absolute; bottom:0; border-top: 5px solid #000000;}        
        .top {height: 40px; border-top: 2px solid #555555;}
        .top > h2 {margin: 10px 5px; color: #555555;}
    </style>
</head>
<body>
    <div class="wrapper">    
        <div class="top">
           <h2 style="text-align: center;">Town of Cobourg Inventory Of Available Land And Buildings</h2>
        </div>
        <div class="header">
            <p style="text-align: center;font-size: 0.9em;">
            For details on any of these properties contact the Business and Tourism Centre<br>
            212 King Street West &bull; Cobourg, Ontario K9A 2N1<br>
            Phone 905-372-5481 &bull; Toll Free: 1-888-262-6874
            </p>    
        
    </div>
        <div>
            @yield('address')
        </div>
        <div class="body">
            @yield('content')
        </div>
        <div class="footer">
            @yield('footer')
        </div>
    </div>
</body>
</html>