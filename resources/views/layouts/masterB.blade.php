<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Northumberland Site Selector APP</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans%7COxygen%7CRoboto+Slab:700%7COswald%7CRoboto' rel='stylesheet'>			 	 	 
</head>
<body>

<div class="wrapper">
	<nav class="navbar navbar-default navbar-inverse" role="navigation">	
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Northumberland Site Selector</a>
		</div>	
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/"> <i class="fa fa-home"></i> Home</a></li>
				<li><a href="/selector/print"> <i class="fa fa-download"></i> Download All Listings</a></li>						
			</ul>		
		</div>
	</nav>

	<section id="main-container">
		<div class="container-wrapper">
			@yield('content')
		</div>
	</section>

	<footer>
		<div class="footer-left">&copy; {!! date('Y') !!}</div>
	</footer>


</div>
	
	{!! Html::style('/css/masterB.css') !!}
	{!! Html::style('/bower_components/font-awesome/css/font-awesome.css') !!}
	{!! Html::style('/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('/bower_components/typicons/src/font/typicons.min.css') !!}
	{!! Html::style('/bower_components/sweetalert/dist/sweetalert.css') !!}
	
	{!! Html::script('/bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('/bower_components/sweetalert/dist/sweetalert-dev.js') !!}
	
	{!! Html::script('/js/selector.js') !!}
	
    @yield('map_script')
    @yield('footer-scripts')
	
</body>

</html>