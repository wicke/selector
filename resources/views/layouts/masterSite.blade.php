<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Northumberland Site Selector APP</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans%7COxygen%7CRoboto+Slab:700%7COswald' rel='stylesheet' type='text/css'>	
</head>
<body>

<div class="wrapper ">
	
<nav class="navbar navbar-default navbar-inverse" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Northumberland Site Selector (DEMO)</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="/selector"> <i class="fa fa-home"></i> Home</a></li>
			<li class="active"><a href="/selector/search"><i class="fa fa-search"></i> Search</a></li>
			
		</ul>
		
	</div><!-- /.navbar-collapse -->
</nav>



<section id="main-container">
	<div class="container-wrapper">
		@yield('content')
	</div>
</section>

<footer>
	<div class="footer-left">&copy; {!! date('Y') !!}</div>
</footer>


</div>

	









	{!! Html::style('/css/masterB.css') !!}
	{!! Html::style('/bower_components/font-awesome/css/font-awesome.css') !!}
	{!! Html::style('/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
	{!! Html::style('/bower_components/typicons/src/font/typicons.min.css') !!}
	{!! Html::script('/bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
	{!! Html::script('/js/master.js') !!}
	{!! Html::script('/js/selector.js') !!}


</body>
</html>