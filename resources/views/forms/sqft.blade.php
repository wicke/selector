<div class="apm-box"  style="padding: 0;background:#efefef; overflow: hidden;clear:both;">
	<div style="width: 50%; float:left; margin: 0;border-right: 1px dashed #555;">		
		<section>
			<span class="search-box-title">Sq. Ft</span>
	   		@foreach($sqft as $s)				
				<label {!! ($data['selected_range'] == $s->id ? 'class="label-selected"' : '') !!}>
		 			<input type="radio" onclick="form.submit();"  name="sq_ft" value="{!! $s->id !!}" {!! ($data['selected_range']  == $s->id ? 'checked' : '') !!}> {!! $s->min !!} - {!! $s->max !!}
				</label>
			@endforeach
			  	<label  {!! ($data['selected_range']  == 'B' ? 'class="label-selected"' : '') !!}>
                    <input type="radio" onclick="form.submit();"  name="sq_ft" value="B" {!! ($data['selected_range']  == "B" ? 'checked' : '') !!}> Include All
                </label>
			
		</section>									
	</div>
	<div style="width: 50%; float:left; margin: 0;">
		
		<section>
			<span class="search-box-title">Acres</span>
	   		@foreach($acreage as $a)				
				<label {!! ($data['selected_acreage'] == $a->id ? 'class="label-selected"' : '') !!}>
		 			<input type="radio" onclick="form.submit();"  name="acreage" value="{!! $a->id !!}" {!! ($data['selected_acreage'] == $a->id ? 'checked' : '') !!}> {!! $a->min !!} - {!! $a->max !!}
				</label>
			@endforeach
				<label  {!! ($data['selected_acreage'] == 'B' ? 'class="label-selected"' : '') !!}>
                    <input type="radio"  onclick="form.submit();" name="acreage" value="B" {!! ($data['selected_acreage'] == "B" ? 'checked' : '') !!}> Include All
                </label>
			
		</section>		
	</div>
</div>