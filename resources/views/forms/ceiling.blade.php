<div class="apm-box"  style="padding: 10px 0px;background:#efefef;min-height: 100px; overflow: hidden;clear:both;">
    <div style="width: 45%; float:left; margin: 0 5px;border-right: 1px dashed #555;">
        <h5>Ceiling Height <small>in development</small></h5>         
        <section>                            
            <label>
                <input type="radio"  name="ceiling" value=""> 0 - 20
            </label>
            <label>
                <input type="radio"  name="ceiling" value=""> 20 - 100
            </label>
      
            <label>
                <input type="radio"  name="ceiling" value="B" {!! ($data['selected_range'] == "B" ? 'checked' : '') !!}> Include All
            </label>        
        </section>                                  
    </div>
    <div style="width: 40%; float:left; margin: 0 5px;">
        <h5>Other <small>in development</small></h5>  
         <label>
            <input type="checkbox"  name="other" value="1"> Loading Docks
        </label>       
        <label>
            <input type="checkbox"  name="other" value="2"> Other Feature
        </label>       
        <label>
            <input type="checkbox"  name="other" value="3"> Other Feature
        </label>       
    </div>
</div>