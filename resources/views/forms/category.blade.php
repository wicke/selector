<div class="apm-box"  style="padding: 0;background:#efefef; overflow: hidden;clear:both;">
    <div style="width: 50%; float:left;margin:0; padding-right:0px; border-right: 1px dashed #555;">            
        <section>
            <span class="search-box-title">Category</span>
            @foreach($categories as $cat)       
            <label {!! ($data['category'] == $cat->id ? 'class="label-selected"' : '') !!}>
                <input  type="radio"  name="category_id" value="{!! $cat->id !!}" {!! ($data['category'] == $cat->id ? 'checked' : '') !!}> {!! $cat->category !!}
            </label>
            @endforeach          
            <label  {!! ($data['category'] == 'B' ? 'class="label-selected"' : '') !!}>
                <input  type="radio"  name="category_id" value="B" {!! ($data['category'] == "B" ? 'checked' : '') !!}> Include All
            </label>        
        </section>
    </div>
    <div style="width: 50%; float:left;">       
        <section>
             <span class="search-box-title">Sub Category</span>
            @foreach($subcategories as $scat)       
            <label {!! ($data['subcategory'] == $scat->id ? 'class="label-selected"' : '') !!}>
                 <input type="radio"  name="subcategory_id" value="{!! $scat->id !!}" {!! ($data['subcategory'] == $scat->id ? 'checked' : '') !!}> {!! $scat->subcategory !!}
            </label>
            @endforeach                
            <label {!! ($data['subcategory']== 'B' ? 'class="label-selected"' : '') !!}>
                 <input  type="radio"  name="subcategory_id" value="B" {!! ($data['subcategory'] == "B" ? 'checked' : '') !!}> Include All
            </label>           
        </section>
    </div>
</div>