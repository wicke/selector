
<div class="content-box" style="margin-top:10px;">    
    <table class="table table-condensed table-hover result-table">
        <thead>
            <tr>
                <th>Address</th>
                <th>Town</th>
                <th>Category</th>
                <th>Sale / Lease</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sites as $site)
            <tr>
                <td data-col="address">
                    <a href="/site/{!! $site->id !!}">
                    @if($site->pic)
                        <img src="/{!! $site->pic !!}" alt="#" class="img-responsive">
                    @else 
                        <img src="/img/site/cobourg.jpg" alt="#" class="img-responsive">
                    @endif
                    </a>
                    {!! $site->address !!} 
                </td>
                <td><a href="">{!! $site->town !!}</a> </td>
                <td><a href="#">{!! $site->category->category !!} {!! $site->subcategory->subcategory !!}</a> </td>
                <td><a href="#">{!! ($site->for_sale == 1 ? 'FOR SALE' : '') !!} {!! ($site->for_lease == 1 ? 'FOR LEASE' : '') !!}</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@foreach($sites as $site)
    <div class="row" style="margin: 0; background: #ffffff; padding: 10px; border: 1px solid #dedede;">
        <div class="col-md-4">
            <a href="/site/{!! $site->id !!}">
                @if($site->pic)
                    <img src="/{!! $site->pic !!}" alt="#" class="img-responsive">
                @else 
                    <img src="/img/site/site4.jpg" alt="#" class="img-responsive">
                @endif

            </a>
        </div>
        <div class="col-md-6">
            <h2 >{!! $site->address !!} - Cobourg</h2>
            <div>
                <p style="font-weight: bold;">{!! ($site->for_sale == 1 ? 'FOR SALE' : '') !!}</p>      
                <p style="font-weight: bold;">{!! ($site->for_lease == 1 ? 'FOR LEASE' : '') !!}</p>    
                <p style="font-style: italic;">Price To Be Discussed</p>    
                <p>
                    {!! $site->description !!}
                </p>                                                
                <p style="font-size: 12px; font-style: italic; font-weight: bold;">{!! $site->category->category !!} {!! $site->subcategory->subcategory !!}</p>            
                <p>min sq ft:{!! $site->min_sq_ft !!} <br />max sq ft: {!! $site->max_sq_ft !!} ACRES: {!! $site->acres; !!}</p>
            </div>                      
        </div>
        <div class="col-md-2">
                <a href="/site/{!! $site->id !!}" class="btn btn-default btn-lg" style="float:right;">VIEW</a>
        </div>
    </div>

@endforeach