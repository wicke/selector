
<div class="row" style="background: #ffffff; padding: 10px; border:1px solid #dedede;">

{!! Form::open(
    ['method' => 'GET',
    'url' => 'selector/s',
    'id' => 'formid', 
    'class' => 'form-horizontal',
    'role' => 'form']) 
!!}



<!-- TOWNS -->
<div class="apm-box half-box" style="padding: 10px 0; background:#efefef;min-height: 100px; overflow: hidden;"> 
    <div style="width: 40%; float:left; margin: 0 10px;">
        <label class="selected"><input type="checkbox"  name="location" value="cobourg" checked> Cobourg!</label>
        <label class="disabled-label"><input type="checkbox"  name="location" value="porthope" disabled class="disabled">  Port Hope</label>
        <label class="disabled-label"><input type="checkbox"  name="location" value="cramahe" disabled> Cramahe</label>
    </div>
    <div style="width: 40%; float:left;">
        <label class="disabled-label"><input type="checkbox"  name="location" value="trenthills" disabled> Trent Hills</label>
        <label class="disabled-label"><input type="checkbox"  name="location" value="" disabled> -- ETC --</label>
    </div>
</div>


<!-- PURCHASE / LEASE -->
<div class="apm-box half-box" style="padding: 10px 0; background:#efefef;min-height: 50px; overflow: hidden;"> 

    <div style="width: 40%; float:left; margin: 0 10px;">
        <label {!! ($for_sale[1] == 1 ? 'class="label-selected"' : '') !!}>
            <input type="checkbox"  name="for_sale[1]" value="1" {!! ($for_sale[1] == 1 ? 'checked' : '') !!}> Purchase
        </label>    
    </div>
    <div style="width: 40%; float:left;">
        <label {!! ($for_sale[0] == 1 ? 'class="label-selected"' : '') !!}>
            <input type="checkbox"  name="for_sale[0]" value="1" {!! ($for_sale[0] == 1 ? 'checked' : '') !!}> Lease
        </label>      
    </div>


</div>    



<!-- CAT / SCATS -->

    @include('forms.category')

<!-- SQUARE FOOTAGE -->

    @include('forms.sqft')

     @include('forms.ceiling')
    

<div class="col-sm-offset-1 col-sm-9">
    {!! Form::button('<i class="fa fa-search"></i> Search', ['type' => 'submit', 'class' => 'btn btn-primary form-control']) !!}
</div>

{!! Form::close() !!}

</div>