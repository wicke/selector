@foreach($sites as $site)
    <div class="row" style="margin: 0; background: #ffffff; padding: 10px; border: 1px solid #dedede;">
        <div class="col-md-4">
            <a href="/site/{!! $site->id !!}">
                @if($site->pic)
                    <img src="/{!! $site->pic !!}" alt="#" class="img-responsive">
                @else 
                    <img src="/img/site/site4.jpg" alt="#" class="img-responsive">
                @endif

            </a>
        </div>
        <div class="col-md-6">
            <h2 >{!! $site->address !!} - Cobourg</h2>
            <div>
                <p style="font-weight: bold;">{!! ($site->for_sale == 1 ? 'FOR SALE' : '') !!}</p>      
                <p style="font-weight: bold;">{!! ($site->for_lease == 1 ? 'FOR LEASE' : '') !!}</p>    
                <p style="font-style: italic;">Price To Be Discussed</p>    
                <p>
                    {!! $site->description !!}
                </p>                                                
                <p style="font-size: 12px; font-style: italic; font-weight: bold;">{!! $site->category->category !!} {!! $site->subcategory->subcategory !!}</p>            
            </div>                      
        </div>
        <div class="col-md-2">
                <a href="/site/{!! $site->id !!}" class="btn btn-default btn-lg" style="float:right;">VIEW</a>
        </div>
    </div>

@endforeach