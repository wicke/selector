@extends('layouts.masterB')

@section('content')    
     <div id="app">
        <div class="col-sm-2">          
            <div class="row" style="padding: 0 2px;">
                @include('selector.searchX')               
            </div>
        </div>
         <!-- RESULTS -->
        <div class="col-sm-10">      
            @if (session('error'))
                <div class="flash-message">
                    <div class="alert alert-success">
                    </div>
                </div>
            @endif           
            <div class="content-box" style="margin-top:10px;">            
                @if($sites->count())        
                    <table class="table table-condensed table-hover result-table table-striped">
                    <thead>
                        <tr>
                            <th>ADDRESS</th>
                            <th>TOWN</th>
                            <th>CATEGORY</th>
                            <th>SALE / LEASE</th>                          
                            <th>SQUARE FOOTAGE!</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        @foreach($sites as $site)
                        <tr>
                            <td data-col="address">
                                <a href="/site/{!! $site->id !!}">
                                    <span class="result-content">                                                                                        
                                    {!! $site->address !!} 
                                    </span>                                    
                                </a>
                            </td>                           
                            <td><a href=""><span class="result-content">{!! $site->town !!}</span></a> </td>                                                        
                            <td><a href="#"><span class="result-content">{!! $site->category->category !!} {!! $site->subcategory->subcategory !!} </span></a> </td>                                                
                            <td><a href="#"><span class="result-content">{!! ($site->for_sale == 1 ? 'FOR SALE' : '') !!} {!! ($site->for_lease == 1 ? 'FOR LEASE' : '') !!}</span></a></td>                            
                            <td><a href="#"><span class="result-content">
                                     {!! $site->min_sq_ft  !!}  {!! ($site->max_sq_ft > $site->min_sq_ft ? ' - ' . $site->max_sq_ft : '')  !!}</span>
                                </a>
                            </td>                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>       
                @else
                    <div>
                        <h4 class="text-center list-group-item-heading">Your search has no results</h4>
                    </div>  
                @endif            
            </div>
        </div>
    </div>
@stop

