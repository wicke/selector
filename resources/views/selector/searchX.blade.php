 {!! Form::open(
    ['method' => 'GET',
    'url' => 'selector',
    'id' => 'formid', 
    'class' => 'form-horizontal',
    'role' => 'form']) 
!!}

    <!-- LOCATION -->
     <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
        <section>
            <span class="search-box-title">Location </span>                                        
            <label class="{!! (old('location') == 'cobourg' ? 'label-selected' : '') !!}">
                <input  type="radio" onclick="form.submit();"  name="location" value="cobourg" {{ (old('location') == 'cobourg' ? ' checked ' : '') }} > Cobourg
            </label>                                                                                                  
        </section>
    </div>

    <!-- FOR SALE -->
    <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
       <section>
            <span class="search-box-title">For Sale or Lease </span>                                        
             <label class="{!! (old('transaction') == 1 ? ' label-selected ' : '') !!}">
                <input  type="radio" onclick="form.submit();"  name="transaction" value="1"  {!! (old('transaction') == 1 ? 'checked' : '') !!}> For Sale
            </label>        
            <label class=" {!! (old('transaction') == 2 ? ' label-selected ' : '') !!}">
                <input  type="radio" onclick="form.submit();"  name="transaction" value="2" {!! (old('transaction') == 2 ? 'checked' : '') !!}> For Lease
            </label>
            <label class=" {!! (old('transaction') == 3 ? ' label-selected ' : '') !!}">
                <input  type="radio" onclick="form.submit();"  name="transaction" value="3" {!! (old('transaction') == 3 ? 'checked' : '') !!}> NO FILTER
            </label>                                                                                            
        </section>
    </div>

    <!-- CATEGORY -->
     <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
       <section>
            <span class="search-box-title">Category </span>    
             @foreach($categories as $cat)       
                <label class="{!! (old('category') == $cat->id ? 'label-selected' : '') !!}">
                    <input  type="radio" onclick="form.submit();"   name="category" value="{!! $cat->id !!}" {!! (old('category') == $cat->id ? 'checked' : '') !!}> {!! $cat->category !!}
                </label>
            @endforeach                                      
             <label  {!! (old('category') == 'B' ? 'class="label-selected"' : '') !!}>
                <input  type="radio" onclick="form.submit();"   name="category" value="B" {!! (old('category') == "B" ? 'checked' : '') !!}>  NO FILTER
            </label>                                                                                                                                            
        </section>
    </div>
    <!-- SUBCATEGORY -->
    <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
       <section>
            <span class="search-box-title">Sub Category </span>    
             @foreach($subcategories as $scat)       
                <label class="{!! (old('subcategory') == $scat->id ? 'label-selected' : '') !!}">
                    <input  type="radio" onclick="form.submit();"   name="subcategory" value="{!! $scat->id !!}" {!! (old('subcategory') == $scat->id ? 'checked' : '') !!}> {!! $scat->subcategory !!}
                </label>
            @endforeach                                      
             <label  {!! (old('subcategory') == 'B' ? 'class="label-selected"' : '') !!}>
                <input  type="radio" onclick="form.submit();"   name="subcategory" value="B" {!! (old('subcategory') == "B" ? 'checked' : '') !!}> NO FILTER
            </label>                                                                                                                                            
        </section>
    </div>
     <!-- SQFT -->
      <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
       <section>
            <span class="search-box-title">Sq. Ft.</span>    
             @foreach($sqft as $s)       
                <label class="{!! (old('sqft') == $s->id ? 'label-selected' : '') !!}">
                    <input  type="radio" onclick="form.submit();"   name="sqft" value="{!! $s->id !!}" {!! (old('sqft') == $s->id ? 'checked' : '') !!}> {!! $s->min !!} - {!! $s->max !!}
                </label>
            @endforeach                                      
             <label  {!! (old('sqft') == 'B' ? 'class="label-selected"' : '') !!}>
                <input  type="radio" onclick="form.submit();"   name="sqft" value="B" {!! (old('sqft') == "B" ? 'checked' : '') !!}> NO FILTER
            </label>                                                                                                                                            
        </section>
    </div>

    <!-- ACRES -->
      <div class="apm-box half-box" style="padding: 0; background:#fff;min-height: 30px; overflow: hidden;"> 
       <section>
            <span class="search-box-title">Acres</span>    
             @foreach($acres as $a)       
                <label class="{!! (old('acres') == $a->id ? 'label-selected' : '') !!}">
                    <input  type="radio" onclick="form.submit();"   name="acres" value="{!! $a->id !!}" {!! (old('acres') == $a->id ? 'checked' : '') !!}> {!! $a->min !!} - {!! $a->max !!}
                </label>
            @endforeach                                      
             <label  {!! (old('acres') == 'B' ? 'class="label-selected"' : '') !!}>
                <input  type="radio" onclick="form.submit();"   name="acres" value="B" {!! (old('acres') == "B" ? 'checked' : '') !!}> NO FILTER
            </label>                                                                                                                                            
        </section>
    </div>

    <div class="col-sm-offset-1 col-sm-9">
        {!! Form::button('<i class="fa fa-search"></i> Search', ['type' => 'submit', 'class' => 'btn btn-primary form-control', 'style' => 'margin: 10px;']) !!}
    </div>
{!! Form::close() !!}