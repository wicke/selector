<div>
	<div class="body-box ">
		<h3 style="padding: 0 0 10px 5px;">ACTIVE INVENTORY</h3>	
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th>Address</th>
					<th></th>
				</tr>
			</thead>
			<tbody>		
				@foreach($sites as $site)
				<tr>
					<td>{!! $site->address !!}</td>
					<td><a href="/admin/site/{!! $site->id !!}" class="btn btn-default btn-xs">Edit</a></td>
				</tr>			
				@endforeach
			</tbody>
		</table>
	</div>
</div>