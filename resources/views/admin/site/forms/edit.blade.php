{!! Form::model($site,
    ['method' => 'POST',
    'route' => ['admin.site.photos', $site->id],
    'id' => 'formid',    
    'class' => 'dropzone form-horizontal',
    'role' => 'form']) 
!!}
	{!! Form::hidden('site_id', $site->id) !!}
{!! Form::close() !!}

{!! Form::model($site,
	['method' => 'PUT',
	'route' => ['admin.site.update', $site->id],
	'id' => 'formid', 
	'class' => 'form-horizontal',
	'role' => 'form']) 
!!}

<div class="col-sm-6">

	<div class="body-box ">
		<h3>SITE</h3>
		<div class="apm-box"  style="padding: 10px 0;background:#efefef;min-height: 100px; overflow: hidden;clear:both;">
			<div style="width: 40%; float:left;margin:0 10px;">		
				<section>
					@foreach($categories as $cat)		
					<label {!! ($site->category_id == $cat->id ? 'class="label-selected"' : '') !!}>
		 				<input type="radio"  name="category_id" value="{!! $cat->id !!}" {!! ($site->category_id == $cat->id ? 'checked' : '') !!} > {!! $cat->category !!}
					</label>
					@endforeach			
				</section>
			</div>
			<div style="width: 50%; float:left;">		
				<section>
					@foreach($subcategories as $scat)		
					<label {!! ($site->subcategory_id == $scat->id ? 'class="label-selected"' : '') !!}>
			 			<input type="radio"  name="subcategory_id" value="{!! $scat->id !!}" {!! ($site->subcategory_id == $scat->id ? 'checked' : '') !!} > {!! $scat->subcategory !!}
					</label>
					@endforeach			
				</section>
			</div>
		</div>

		<div class="apm-box"  style="padding: 10px 0;background:#efefef;min-height: 50px; overflow: hidden;clear:both;">
			<div style="width: 40%; float:left;margin:0 10px;">		
				<section>							
					<label {!! ($site->for_sale == 1 ? 'class="label-selected"' : '') !!} >
						{!! Form::hidden('for_sale', 0) !!}				
						{!! Form::checkbox('for_sale', 1) !!} For Sale
		 			
					</label>				
				</section>
			</div>
			<div style="width: 50%; float:left;">		
				<section>							
					<label {!! ($site->for_lease == 1 ? 'class="label-selected"' : '') !!} >
			 			{!! Form::hidden('for_lease', 0) !!}				
						{!! Form::checkbox('for_lease', 1) !!} For Lease
					</label>					
				</section>
			</div>
		</div>


		<div class="form-group">
			{!! Form::label('address', 'Address', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('address', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('record', 'Record #', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('record', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('location', 'Location', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('location', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('mls', 'MLS', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('mls', null, ['class' => 'form-control']) !!}
			</div>
		</div>
	</div>

	<div class="body-box ">
		<h3>SERVICE</h3>	
		<div class="form-group">	
			<div class="col-sm-9">		
				{!! Form::select('service_id', $service_list, null, ['class' => 'form-control']) !!}
			</div>
		</div>					
	</div>

	<div class="body-box ">
		<h3>ZONING</h3>			
		<div class="form-group">	
			<div class="col-sm-9">		
				{!! Form::select('zoning_id', $zoning_list, null, ['class' => 'form-control']) !!}
			</div>
		</div>		
	</div>

	<div class="body-box ">
		<h3>PIC</h3>			
		<div class="form-group">	
			<div class="col-sm-9">		
				<img src="{!! '../../../uploads/' . $site->id . '/' . $site->pic !!}" alt="" style="width:100px;" >
			</div>
		</div>		
	</div>

</div>

<div class="col-sm-6">
	
	<div class="body-box ">
		<h3>STATUS</h3>
		<div class="apm-box" style="padding: 10px 0;background:#efefef;min-height: 50px; overflow: hidden;clear:both;">			
			<section>			
				<div>				
					<label  style="width:31%;float:left;"  class="{!! ($site->status == 'active' ?  'checkbox-inline label-selected' : 'checkbox-inline' ) !!}">		
		 				<input type="radio"  name="status" value="active" {!! ($site->status == 'active' ? 'checked' : '') !!} > Active			 	
					</label>
				
					<label style="width:31%;float:left;"  class="{!! ($site->status == 'dormant' ?  'checkbox-inline label-selected' : 'checkbox-inline' ) !!}">
		 				<input type="radio"  name="status" value="dormant" {!! ($site->status == 'dormant' ? 'checked' : '') !!} > Dormant
					</label>
				
					<label style="width:31%;float:left;"  class="{!! ($site->status == 'incomplete' ?  'checkbox-inline label-selected' : 'checkbox-inline' ) !!}">
		 				<input type="radio"  name="status" value="incomplete" {!! ($site->status == 'incomplete' ? 'checked' : '') !!} > Incomplete
					</label>
				</div>			
			</section>
		</div>
	</div>

	<div class="body-box ">
		<h3>CONTACT</h3>	
		@if($site->contact)
			@foreach($site->contact as $contact)
			<table class="table">
				<tr>
					<td>Contact</td>
					<td>{!! $contact->first_name !!}  {!! $contact->last_name !!} <br></td>
				</tr>
				<tr>
					<td>Company</td>
					<td>{!! $contact->company !!}<br></td>
				</tr>
				<tr>
					<td>City</td>
					<td>{!! $contact->city !!}<br></td>
				</tr>
				<tr>
					<td>Phone</td>
					<td>{!! $contact->phone !!}<br></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>{!! $contact->email !!}<br></td>
				</tr>
				<tr>
					<td></td>
					<td>{!! $contact->isRealtor !!}</td>
				</tr>
			</table>
		  	@endforeach	  
		 @endif		 
			<a href="#" class="btn btn-default btn-sm">Edit</a>
		
	</div>

	

	<div class="body-box ">
		<h3>DESCRIPTION</h3>			
		<div class="form-group">	
			<div class="col-sm-12">									
				{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '5']) !!}				
			</div>
		</div>		
	</div>

	<div class="body-box ">
		<h3>PUBLIC COMMENTS</h3>			
		<div class="form-group">	
			<div class="col-sm-12">									
				{!! Form::textarea('public_comments', null, ['class' => 'form-control', 'rows' => '5']) !!}				
			</div>
		</div>		
	</div>

	<div class="body-box ">
		<h3>INTERNAL COMMENTS</h3>			
		<div class="form-group">	
			<div class="col-sm-12">								
				{!! Form::textarea('internal_comments', null, ['class' => 'form-control', 'rows' => '5']) !!}				
			</div>
		</div>		
	</div>

	
	
			<div  class="center-block" style="width: 50%;">	
				{!! Form::submit('SAVE', ['class' => 'btn btn-primary btn-lg btn-block']) !!}
			</div>
	
</div>


{!! Form::close() !!}



