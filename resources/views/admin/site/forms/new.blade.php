{!! Form::open(
	['method' => 'POST',
	'route' => ['admin.site.store'],
	'id' => 'formid', 
	'class' => 'form-horizontal',
	'role' => 'form']) 
!!}


<div class="col-sm-6">



	<div class="body-box ">
		<h3>SITE</h3>
		<div class="apm-box"  style="padding: 10px 0;background:#efefef;min-height: 100px; overflow: hidden;clear:both;">
			<div style="width: 40%; float:left;margin:0 10px;">		
				<section>
					<small>Category</small>
					@foreach($categories as $cat)		
					<label >
		 				<input type="radio"   name="category_id" value="{!! $cat->id !!}" > {!! $cat->category !!}
					</label>
					@endforeach			
				</section>
			</div>
			<div style="width: 50%; float:left;">		
				<section>
					<small>Subcategory</small>
					@foreach($subcategories as $scat)		
					<label>
			 			<input type="radio"  name="subcategory_id" value="{!! $scat->id !!}" > {!! $scat->subcategory !!}
					</label>
					@endforeach			
				</section>
			</div>
		</div>

		<div class="apm-box"  style="padding: 10px 0;background:#efefef;min-height: 50px; overflow: hidden;clear:both;">
			<div style="width: 40%; float:left;margin:0 10px;">		
				<section>							
					<label >
		 				<input type="checkbox"   name="for_sale" value="1" > For Sale
					</label>				
				</section>
			</div>
			<div style="width: 50%; float:left;">		
				<section>							
					<label>
			 			<input type="checkbox"  name="for_lease" value="1" > For Lease
					</label>					
				</section>
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('address', 'Address', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('address', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('record', 'Record #', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('record', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('location', 'Location', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('location', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('mls', 'MLS', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('mls', null, ['class' => 'form-control']) !!}
			</div>
		</div>
	</div>




	<div class="body-box ">
		<h3>STATUS</h3>
		<div class="apm-box" style="padding: 10px 0;background:#efefef;min-height: 50px; overflow: hidden;clear:both;">			
			<section>			
				<div>				
					<label class="checkbox-inline" style="width:31%;float:left;">
		 				<input type="radio"   name="status_id" value="1" > Active			 	
					</label>
				
					<label class="checkbox-inline" style="width:31%;float:left;">
		 				<input type="radio"  name="status_id" value="2" > Dormant
					</label>
				
					<label class="checkbox-inline" style="width:31%;float:left;">
		 				<input type="radio"  name="status_id" value="3" > Incomplete
					</label>
				</div>			
			</section>
		</div>
	</div>




	<div class="body-box ">
		<h3>SERVICE</h3>	
		<div class="form-group">	
			<div class="col-sm-9">		
				{!! Form::select('service_id', $service_list, null, ['class' => 'form-control']) !!}
			</div>
		</div>					
	</div>

	


	<div class="body-box ">
		<h3>ZONING</h3>			
		<div class="form-group">	
			<div class="col-sm-9">		
				{!! Form::select('zoning_id', $zoning_list, null, ['class' => 'form-control']) !!}
			</div>
		</div>		
	</div>


	<div class="body-box ">
		<h3>PHOTOS</h3>			
		<div class="form-group">	
			<div class="col-sm-9">						
			</div>
		</div>		
	</div>


</div>



<div class="col-sm-6">

	<div class="body-box ">
		<h3>CONTACT</h3>			
		<div class="form-group">	
			<div class="col-sm-9">		
				{!! Form::select('person_id', $people_list, null, ['class' => 'form-control']) !!}
			</div>
		</div>	
		<div class="apm-box" style="padding: 10px 0;background:#efefef;min-height: 50px; overflow: hidden;clear:both;">			
		<section>			
				<div style="width:50%;">				
					<label>
		 				<input type="checkbox"   name="isRealtor" value="1" > Realtor			 	
					</label>
				
					<label>
		 				<input type="checkbox"  name="isDeveloper" value="2" > Developer
					</label>
				
					<label>
		 				<input type="checkbox"  name="isPropertyMgmt" value="3" > Property Management
					</label>

					<label>
		 				<input type="checkbox"  name="isOwner" value="3" > Owner
					</label>

					<label>
		 				<input type="checkbox"  name="contact" value="3" > Contact
					</label>



				</div>			
			</section>	
		</div>
	</div>

<div class="body-box ">
		<h3>DESCRIPTION</h3>			
		<div class="form-group">	
			<div class="col-sm-12">									
				{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '5']) !!}				
			</div>
		</div>		
	</div>
	

	<div class="body-box ">
		<h3>PUBLIC COMMENTS</h3>			
		<div class="form-group">	
			<div class="col-sm-12">									
				{!! Form::textarea('public_comments', null, ['class' => 'form-control', 'rows' => '5']) !!}					
			</div>
		</div>		
	</div>

	<div class="body-box ">
		<h3>INTERNAL COMMENTS</h3>			
		<div class="form-group">	
			<div class="col-sm-12">								
				{!! Form::textarea('internal_comments', null, ['class' => 'form-control', 'rows' => '5']) !!}				
			</div>
		</div>		
	</div>
		
	<div  class="center-block" style="width: 50%;">	
		{!! Form::submit('SAVE', ['class' => 'btn btn-primary btn-lg btn-block']) !!}
	</div>

</div>


{!! Form::close() !!}