@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Inventory</h1>
@stop

@section('page-nav')
	@include('admin.site.nav')	
@stop


@section('content')

	<div class="col-sm-6">
		<div class="">
			<a href="/admin/site/create" class="btn btn-default btn-lg btn-block"><i class="fa fa-plus-circle"></i> NEW SITE</a>
		</div>
		@include('admin.site.sitelist')

	</div>

	<div class="col-sm-6">	
	</div>


@stop