@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Inventory</h1>
@stop

@section('page-nav')
	@include('admin.site.nav')	
@stop


@section('content')
	@include('admin.site.forms.edit')
@stop


@section('footer.scripts')
    <link rel="stylesheet" href="/css/dropzone.css">  
     {!! Html::script('/js/dropzone.js') !!}
@stop