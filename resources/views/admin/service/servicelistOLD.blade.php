<div ng-controller="ServiceCtrl as vm" >	
	<div class="body-box">	
		<h3 style="padding: 0 0 10px 5px;">AVAILABLE SERVICES !!!!</h3>	

		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th>Service Code</th>
					<th>Service</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="service in vm.services">
					<td><% service.service_code %></td>
					<td><% service.service_description %></td>
					<td>
		                {!! Form::open(
		                	['method' => 'DELETE',
		                	'route' => ['admin.service.destroy',  ],
		                	'class' => 'pull-right'
		                	]) 
		                !!}		                		               
		                 <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> Delete</button>
		                {!! Form::close() !!}

		                <button ng-click="remove(service)">DELETE</button>
		                <a href="/admin/service/<% service.id %>" class="btn btn-default btn-xs pull-right form-button"><i class="fa fa-pencil"></i> Edit</a>
		             </td>
				</tr>
			</tbody>
		</table>	
	</div>
</div>