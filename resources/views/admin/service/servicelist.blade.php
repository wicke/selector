<div>	
	<div class="body-box">	
		<h3 >AVAILABLE SERVICES</h3>	

		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th>Code</th>
					<th>Service</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($services as $service)
				<tr>
					<td>{!! $service->service_code !!}</td>
					<td>{!! $service->service_description !!}</td>
					<td>
		                {!! Form::open(
		                	['method' => 'DELETE',
		                	'route' => ['admin.service.destroy', $service->id ],
		                	'class' => 'pull-right'
		                	]) 
		                !!}		                		               
		                 <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> Delete</button>
		                {!! Form::close() !!}		              
		                <a href="/admin/service/{!! $service->id !!}" class="btn btn-default btn-xs pull-right form-button"><i class="fa fa-pencil"></i> Edit</a>
		             </td>
				</tr>
				@endforeach
			</tbody>
		</table>	
	</div>
</div>