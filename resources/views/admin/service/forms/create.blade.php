{!! Form::open(
	['method' => 'POST',
	'route' => ['admin.service.store'],
	'id' => 'formid', 
	'class' => 'form-horizontal',
	'role' => 'form']) 
!!}

<div class="col-sm-6">

	<div class="body-box ">
		<h3>NEW SERVICE</h3>
		
		<div class="form-group">
			{!! Form::label('service_id', 'Service ID', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('service_id', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('service_code', 'Code', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('service_code', null, ['class' => 'form-control']) !!}
			</div>
		</div>

		<div class="form-group">
			{!! Form::label('service_description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
			<div class="col-sm-9">
				{!! Form::text('service_description', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		
	</div>
	<div  class="center-block" style="width: 50%;">	
		{!! Form::submit('SAVE', ['class' => 'btn btn-primary btn-lg btn-block']) !!}
	</div>
	{!! Form::close() !!}
</div>



<div class="col-sm-6">	
	@include('admin.service.servicelist')		
</div>






