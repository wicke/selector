@extends('admin.layout.master')


@section('header')
	<h1 class="header-title">Services</h1>
@stop


@section('page-nav')
	@include('admin.service.nav')	
@stop


@section('content-nav')
	{{-- expr --}}
@stop


@section('content')
	
<div class="col-sm-6">	
	@include('admin.service.servicelist')		
</div>


@stop

