<div class="nav-wrapper">
	<div class="nav-header">
		<h1>COBOURG</h1>
	</div>
	<div>
		<a href="/admin/site" class="nav-button"><i class="fa fa-building"> </i> Sites</a>				
	</div>
	<ul class="nav" id="side-menu">		
		<li>
			<a href="{{ URL('/admin') }}"><i class="fa fa-home"></i><span class="nav-label"> Home</span></a>
		</li>
		<li>
			<a href="{{ URL('/admin/logout') }}"><i class="fa fa-times" aria-hidden="true"></i><span class="nav-label"> Logout</span></a>
		</li>
	</ul>
</div>