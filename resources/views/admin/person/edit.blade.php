@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Contact</h1>
@stop

@section('page-nav')
	<div class="page-nav">
		<ul class="page-nav-list">
			<li><a href="/admin/person/create"> <i class="fa fa-plus-circle"></i> New Person</a></li>						
		</ul>
	</div>
@stop


@section('content')
	@include('admin.person.forms.edit')
@stop