@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Contacts</h1>
@stop

@section('page-nav')
	
@stop


@section('content')

	<div class="col-sm-6">
		<div class="">
			<a href="/admin/person/create" class="btn btn-default btn-lg btn-block"><i class="fa fa-plus-circle"></i> NEW CONTACT</a>
		</div>
	</div>

	<div class="col-sm-6">
		<div class="body-box">
			<input ng-model="searchContact" type="search" class="form-control"  placeholder="Search Contacts" />
		</div>	
		
		@include('admin.person.peoplelist')
	</div>


@stop