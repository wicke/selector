<div class="col-sm-6">
	<div class="body-box ">
		<h3>NEW CONTACT</h3>
		{!! Form::open(
			['method' => 'POST',
			'route' => ['admin.person.store'],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.person.forms.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.person.peoplelist')	
</div>