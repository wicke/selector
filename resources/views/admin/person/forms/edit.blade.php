<div class="col-sm-6">
	<div class="body-box ">
		<h3>CONTACT</h3>
		{!! Form::model($person,
			['method' => 'PUT',
			'route' => ['admin.person.update', $person->id],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.person.forms.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.person.peoplelist')	
</div>