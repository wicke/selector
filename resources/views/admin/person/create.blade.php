@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Contact</h1>
@stop

@section('page-nav')
	
@stop

@section('content')						
	@include('admin.person.forms.create')					
@stop