@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Sub Categories</h1>
@stop

@section('page-nav')
	<div class="page-nav">
		<ul class="page-nav-list">
			<li><a href="/admin/subcategories"> <i class="fa fa-plus-circle"></i> New Sub Category</a></li>						
		</ul>
	</div>
@stop

@section('content')
	@include('admin.subcategory.form.create')
@stop