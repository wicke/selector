<div class="col-sm-6">
	<div class="body-box ">
		<h3>SUBCATEGORY</h3>
		{!! Form::model($cat,
			['method' => 'PUT',
			'route' => ['admin.subcategories.update', $cat->id],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.subcategory.form.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.subcategory.subcategorylist')	
</div>