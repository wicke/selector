<div class="col-sm-6">
	<div class="body-box ">
		<h3>NEW SUBCATEGORY</h3>
		{!! Form::open(
			['method' => 'POST',
			'route' => ['admin.subcategories.store'],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.subcategory.form.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.subcategory.subcategorylist')	
</div>