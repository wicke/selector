<div class="form-group">
	{!! Form::label('subcategory', 'Sub Category', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-9">
		{!! Form::text('subcategory', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-3">
		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
	</div>
</div>	