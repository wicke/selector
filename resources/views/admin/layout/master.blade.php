<!DOCTYPE html>
<html ng-app="selectorApp">
<head>
	<meta charset="utf-8">	
	<title>ADMIN</title>
	<link href='http://fonts.googleapis.com/css?family=Lato|Open+Sans|Oswald' rel='stylesheet' >	
</head>
<body>

<div id="wrapper">	
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">												
			@include('admin.nav')				
		</div>
	</nav>		
	<div id="page-wrapper" class="gray-bg dashbard-1 no-pad">		
		<div class="header">
			@yield('header')
		</div>
		@yield('page-nav')	
		@yield('content-nav')	
		<div class="content-wrapper">
			@yield('content')
		</div>		
		<div class="footer">          
            <div>
                <strong>&copy; {!! date('Y') !!}</strong> 
            </div>
        </div>	
	</div>
</div>


	
	{!! Html::style('bower_components/font-awesome/css/font-awesome.css') !!}		
	{!! Html::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}   	
	{!! Html::style('css/admin.css') !!} 	

	{!! Html::script('bower_components/jquery/dist/jquery.min.js') !!}
	{!! Html::script('bower_components/bootstrap/dist/js/bootstrap.js') !!}
	{!! Html::script('bower_components/angular/angular.min.js') !!}
	{!! Html::script('bower_components/angular-resource/angular-resource.min.js') !!}
	{!! Html::script('js/app/app.js') !!}
	{!! Html::script('js/selector.js') !!}

	{!! Html::script('js/controller/siteController.js') !!}
	{!! Html::script('js/controller/servController.js') !!}
	{!! Html::script('js/service/siteService.js') !!}	
	{!! Html::script('js/service/servService.js') !!}	
	
	@yield('footer.scripts')
	
</body>
</html>	