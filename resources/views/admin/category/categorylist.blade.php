<div class="body-box ">
	<h3>AVAILABLE CATEGORIES</h3>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>Category</th>				
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($category as $c)
			<tr>
				<td>{!! $c->category !!}</td>				
				<td>
	                {!! Form::open(
	                	['method' => 'DELETE',
	                	'route' => ['admin.categories.destroy', $c->id ],
	                	'class' => 'pull-right'
	                	]) 
	                !!}		                		               
	                	 <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> Delete</button>
	                {!! Form::close() !!}		              
		            <a href="/admin/categories/{!! $c->id !!}" class="btn btn-default btn-xs pull-right form-button"><i class="fa fa-pencil"></i> Edit</a>
		        </td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>