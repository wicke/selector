<div class="col-sm-6">
	<div class="body-box ">
		<h3>NEW CATEGORY</h3>
		{!! Form::open(
			['method' => 'POST',
			'route' => ['admin.categories.store'],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.category.form.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.category.categorylist')	
</div>