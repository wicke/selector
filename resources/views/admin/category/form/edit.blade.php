<div class="col-sm-6">
	<div class="body-box ">
		<h3>CATEGORY</h3>
		{!! Form::model($cat,
			['method' => 'PUT',
			'route' => ['admin.categories.update', $cat->id],
			'id' => 'formid', 
			'class' => 'form-horizontal',
			'role' => 'form']) 
		!!}	
			@include('admin.category.form.form')
		{!! Form::close() !!}
	</div>
</div>

<div class="col-sm-6">
	@include('admin.category.categorylist')	
</div>