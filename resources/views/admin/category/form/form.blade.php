<div class="form-group">
	{!! Form::label('category', 'Category', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-9">
		{!! Form::text('category', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-3">
		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
	</div>
</div>	