@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Categories</h1>
@stop

@section('page-nav')
	<div class="page-nav">
		<ul class="page-nav-list">
			<li><a href="/admin/categories"> <i class="fa fa-plus-circle"></i> New Category</a></li>						
		</ul>
	</div>
@stop

@section('content')
	@include('admin.category.form.create')
@stop