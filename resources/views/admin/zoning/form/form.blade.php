<div class="form-group">
	{!! Form::label('zoning_id', 'Zoning ID', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-9">
		{!! Form::text('zoning_id', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('zoning_code', 'Zoning Code', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-9">
		{!! Form::text('zoning_code', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('zoning_description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-9">
		{!! Form::text('zoning_description', null, ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-3">
		{!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
	</div>
</div>	