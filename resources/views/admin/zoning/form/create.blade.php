<div class="col-sm-6">

	<div class="body-box ">
	<h3>NEW ZONING</h3>
	{!! Form::open(
		['method' => 'POST',
		'route' => ['admin.zoning.store'],
		'id' => 'formid', 
		'class' => 'form-horizontal',
		'role' => 'form']) 
	!!}
	
	@include('admin.zoning.form.form')


	{!! Form::close() !!}
</div>
</div>


<div class="col-sm-6">
	@include('admin.zoning.zoninglist')	
</div>