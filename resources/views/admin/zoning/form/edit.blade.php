<div class="col-sm-6">
	
	<div class="body-box ">
	<h3>ZONING</h3>
	{!! Form::model( $zone, [
		'method' => 'PUT',
		'route' => ['admin.zoning.update', $zone->id],
		'class' => 'form-horizontal',
		'role' => 'form'
	]) !!}

		@include('admin.zoning.form.form')

	{!! Form::close() !!}
</div>
</div>


<div class="col-sm-6">
	@include('admin.zoning.zoninglist')
</div>