<div class="body-box ">
	<h3>AVAILABLE ZONING</h3>
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				<th>Code</th>
				<th>Desctiption</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($zoning as $z)
			<tr>
				<td>{!! $z->zoning_code !!}</td>
				<td>{!! $z->zoning_description !!}</td>				
				<td>
		                {!! Form::open(
		                	['method' => 'DELETE',
		                	'route' => ['admin.zoning.destroy', $z->id ],
		                	'class' => 'pull-right'
		                	]) 
		                !!}		                		               
		                 <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-times-circle"></i> </button>
		                {!! Form::close() !!}		              
		                <a href="/admin/zoning/{!! $z->id !!}" class="btn btn-default btn-xs pull-right form-button"><i class="fa fa-pencil"></i> </a>
		             </td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>