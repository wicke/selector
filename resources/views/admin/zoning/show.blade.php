@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Zoning</h1>
@stop

@section('page-nav')
	<div class="page-nav">
		<ul class="page-nav-list">
			<li><a href="/admin/zoning"> <i class="fa fa-plus-circle"></i> New Zoning</a></li>					
		</ul>
	</div>
@stop

@section('content')
	@include('admin.zoning.form.edit')
@stop