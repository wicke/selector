@extends('admin.layout.master')

@section('header')
	<h1 class="header-title">Land And Building Inventory - COBOURG</h1>
@stop

@section('page-nav')
	
@stop


@section('content')




    <div class="body-box" style="min-height:200px;">
        @if(Session::has('status'))
            <div class="alert alert-success">{{ Session::get('status') }}</div>
        @endif
        <div class="col-md-5">
        	{!! Form::open(
                ['method' => 'POST',
                'url' => '/admin/getFile',
                'id' => 'formid', 
                'class' => 'form-horizontal',
                'role' => 'form',
                'files' => true]) 
            !!} 
        
            <div class="form-group">
                {!! Form::label('report', 'FILE', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-9">
                    {!! Form::file('report', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    {!! Form::submit('UPLOAD', ['class' => 'btn btn-primary form-control']) !!}
                </div>
            </div>

            {!! Form::close() !!}


        </div>
      
     
    

@stop