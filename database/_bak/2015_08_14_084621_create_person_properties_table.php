<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonPropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person_properties', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id')->unsigned;
			$table->integer('person_id')->unsigned;
			$table->boolean('isRealtor');
			$table->boolean('isDeveloper');
			$table->boolean('isPropertyMgmt');
			$table->boolean('isOwner');
			$table->boolean('contact');
			$table->integer('person_order');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person_properties');
	}

}
