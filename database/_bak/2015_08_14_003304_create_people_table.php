<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('people', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('person_id');
			$table->string('prefix');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('title');
			$table->string('company');
			$table->string('address');
			$table->string('city');
			$table->string('prov');
			$table->string('pc');
			$table->string('country');
			$table->string('phone');
			$table->string('tollfree');
			$table->string('fax');
			$table->string('email');
			$table->string('cell');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('people');
	}

}
