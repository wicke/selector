<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactSiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contact_site', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('site_id')->unsigned();
			$table->integer('contact_id')->unsigned();
			$table->boolean('isRealtor')->default(0);
			$table->boolean('isDeveloper')->default(0);
			$table->boolean('isPropertyMgmt')->default(0);
			$table->boolean('isOwner')->default(0);
			$table->boolean('isContact')->default(0);
			$table->integer('contact_order')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contact_site');
	}

}
