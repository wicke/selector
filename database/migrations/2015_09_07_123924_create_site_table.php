<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sites', function(Blueprint $table)
		{
			$table->increments('id');			
			$table->string('record'); 
			$table->string('mls');
			$table->string('roll');
			$table->integer('category_id')->unsigned;
			$table->integer('subcategory_id')->unsigned;
			$table->integer('zoning_id')->unsigned;
			$table->integer('service_id')->unsigned;
			$table->string('address');
			$table->text('description'); 
			$table->text('legal_description');
			$table->float('purchase_price');
			$table->string('purchase_price_notes');
			$table->float('lease_orice');
			$table->string('lease_price_term');
			$table->string('lease_price_notes');
			$table->integer('min_sq_ft');
			$table->integer('max_sq_ft');
			$table->integer('total_sq_ft');
			$table->string('distance_centers');
			$table->integer('status');
			$table->text('internal_comments');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sites');
	}

}
