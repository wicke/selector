<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Range extends Model {

    protected $table = 'ranges';

    public  function scopeSqft($query)
    {
        return $query->where('range_cat', '=', 1);
    }

    public  function scopeAcreage($query)
    {
        return $query->where('range_cat', '=', 2);
    }

}