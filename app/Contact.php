<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $table = 'contact';
    protected $fillable = [
        'first_name',
        'last_name',
        'title',
        'company',
        'address',
        'city',
        'prov',
        'pc',
        'phone',
        'email'
    ];

    public function site()
    {
         return $this->belongsToMany('App\Site');
    }

}