<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

	protected $table = 'people';
	protected $fillable = [
		'first_name',
		'last_name',
		'title',
		'company',
		'address',
		'city',
		'prov',
		'pc',
		'phone'
	];

	public function site()
	{
		$this->belongsToMany('App\Site', 'person_properties', 'person_id', 'site_id');
	}

}