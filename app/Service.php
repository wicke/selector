<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {

	protected $table = 'services';
	protected $fillable = [
		'service_id',
		'service_code',
		'service_description'
	];

    public function sites()
    {
        return $this->hasMany('App\Site');
    }
	
}