<?php  namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use App\Filters;
use App\Range;

class SiteFilters extends QueryFilter
{

    public function location($town) //selector.app/selector/?location=cobourg
    {
        return $this->builder->where('town', $town);
    }

    public function transaction($option)  //selector.app/selector/?transaction={1,2,3}
    {            
         if($option == 1)
         {                   
            return $this->builder->where('for_sale', 1);                                
         }
         elseif ($option == 2)
         {      
            return $this->builder->where('for_lease', 1);            
         }
        elseif ($option == 3)
         {                     
            return $this->builder->where(function($q){
                $q->where('for_sale', 1)
                    ->orWhere('for_lease', 1);
            });
         }  
    }

    public function category($category) //selector.app/selector/?category=1
    {
        if($category != "B")      
        {
            return $this->builder->where('category_id', $category);   
        }                
    }

    public function subcategory($subcategory) //selector.app/selector/?subcategory=5
    {
        if($subcategory != "B")      
        {
            return $this->builder->where('subcategory_id', $subcategory);   
        }
    }

    public function sqft($sqft)  //selector.app/selector/?sqft=5
    {
         if($sqft != "B")      
         {
            $sqfts = Range::whereId($sqft)->first();  
            $min = $sqfts->min;
            $max = $sqfts->max;     
            return $this->builder->where(function($q) use ($min, $max) {
                $q->where('min_sq_ft', '<=', $max)
                    ->where('max_sq_ft', '>=', $min);
            });         
         }           
    }

    public function acres($acre)  //selector.app/selector/?acres=5
    {
         if($acre != "B")      
         {
            $acres = Range::whereId($acre)->first();  
            $min = $acres->min;
            $max = $acres->max;     
            return $this->builder->where(function($q) use ($min, $max) {
                $q->where('acres', '<=', $max)
                    ->where('acres', '>=', $min);
            });         
         }         
    }
    
}