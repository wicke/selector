<?php namespace App;

use Maatwebsite\Excel\Files\ExcelFile;

class SiteData extends ExcelFile {
   
    public function getFile()
     {
       return "/data/csv/Site.csv";
    // return storage_path('exports') . '/file.csv';
    //     // Import a user provided file
    //     $file = $request->file('report');
    //     $name = time() . '_' . $file->getClientOriginalName();
    //     $movedFolder = 'data/csv';
    //     $file->move($movedFolder, $name);
    //    $filename = $movedFolder . '/' . $name;
    //     // Return it's location
    //     return $filename;
     }

    public function getFilters()
    {
        return [
            'chunk'
        ];
    }
   
}
