<?php

//--------SELECTOR ROUTES
Route::get('/', 'SelectorController@index');
Route::get('/selector', 'SelectorController@index');
Route::get('/selector/print',  'ReportController@printList');

//--------REPORT ROUTES
//Route::get('/report/all', 'SelectorController@report');

//---------SITE ROUTES
Route::get('/site/{id}', 'SiteController@display');
Route::get('/site/{id}/print', 'ReportController@printsite');

//---------EXCEL ROUTES

//--------------ADMIN ROUTES
Route::group(['namespace' => 'Admin'], function()
{
	Route::get('/admin', 'AdminController@index');
	Route::post('/admin/getFile', 'AdminController@uploadFile');
	Route::resource('/admin/site', 'SiteController');	
	Route::post('/admin/site/photos', ['as' => 'admin.site.photos', 'uses' => 'SiteController@photos']);
	Route::get('/admin/logout', 'AdminController@logout');
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);