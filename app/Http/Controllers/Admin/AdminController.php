<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use Excel;
use App\SiteData;
use App\Site;
use Maatwebsite\Excel\Files\ExcelFile;
use App\Http\Requests\SaveUploadRequest;

class AdminController extends Controller {

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function logout()
  {
    \Auth::logout();
    return redirect('/admin');
  }

  public function index()
	{
    return view('admin.index');
	}

  public function loadFile($file)
  {
      $excel = Excel::load($file, function($reader) { })->get();      
      \DB::table('sites')->truncate();
      foreach ($excel as $key => $value) {                
          $forsale = 0;
          $forlease = 0;
          
          if (($value['purchase_price'] != null && $value['purchase_price'] <> '') || ($value['purchase_price_notes'] <> '')) {
            $forsale = 1;           
          }
          if (($value['lease_price'] != null && $value['lease_price'] <> '') || ($value['lease_price_term'] <> '') ) {
            $forlease = 1;
          }         
          $newrow[] = [
            'id' => $value['id'],
            'record' => $value['record'],            
            'subcategory_id' => $value['subcategory_id'],
            'address' => $value['address'],
            'legal_description' => $value['legal_description'],
            'acres' => $value['acres'],            
            'min_sq_ft' => $value['min_sq_ft'],
            'max_sq_ft' => $value['max_sq_ft'],
            'total_sq_ft' => $value['total_sq_ft'],
            'description' => $value['description'],
            'internal_comments' => $value['internal_comments'],            
            'purchase_price' => $value['purchase_price'],
            'purchase_price_notes'  => $value['purchase_price_notes'], 
            'lease_price' => $value['lease_price'],
            'lease_price_term' => $value['lease_price_term'],
            'lease_price_notes' => $value['lease_price_notes'],
            'distance_centers' => $value['distance_centers'],
            'updated_at' => $value['updated_at'],
            'status' => $value['status'],
            'mls' => $value['mls'],
            'pic' => $value['pic'],
            'zoning_id' => $value['zoning_id'],
            'service_id' => $value['service_id'],           
            'category_id' => $value['category_id'],
            'for_sale' => $forsale,
            'for_lease' => $forlease,
            'town' => 'Cobourg',
            'lat' => $value['lat'],
            'lon' => $value['lon']
          ];

          //COMING FROM ACCESS
          // $newrow[] = [
          //   'id' => $value['idnumber'],
          //   'record' => $value['record'],            
          //   'subcategory_id' => $value['subcategoryid'],
          //   'address' => $value['siteaddress'],
          //   'legal_description' => $value['legaldescription'],
          //   'acres' => $value['acres'],            
          //   'min_sq_ft' => $value['minsqft'],
          //   'max_sq_ft' => $value['maxsqft'],
          //   'total_sq_ft' => $value['totalsqft'],
          //   'description' => $value['description'],
          //   'internal_comments' => $value['comments'],            
          //   'purchase_price' => $value['purchaseprice'],
          //   'lease_price' => $value['leaseprice'],
          //   'distance_centers' => $value['distancecentres'],
          //   'updated_at' => $value['updated'],
          //   'status' => $value['status'],
          //   'mls' => $value['mls'],
          //   'pic' => $value['picturelocation'],
          //   'zoning_id' => $value['zoningid'],
          //   'service_id' => $value['serviceid'],           
          //   'category_id' => $value['categoryid']
          // ];
        
      }

     
      foreach($newrow as $r=>$data)
      {
        Site::create($data);
     
      }
     // dd($newrow);
   
  }


    public function importDataFile(SiteData $import)
    {
        // get the results
         $results = $import->get();
         dd($results);
    }

   public function uploadFile(SaveUploadRequest $request)
    {                
        $file = $request->file('report');        
        $name = time() . '_' . $file->getClientOriginalName();        
        $movedFolder = 'data/csv';
        $file->move($movedFolder, $name);        
        $filename = $movedFolder . '/' . $name;       
        $testing = $this->loadFile($filename);    
        $request->session()->flash('status', 'File uploaded!');   
        return redirect('/admin');   
    }

    public function getFile()
    {
      // Import a user provided file    
       //  $name = time() . '_' . $file->getClientOriginalName();
       //  $movedFolder = 'data/csv';
       //  $file->move($movedFolder, $name);
       // $filename = $movedFolder . '/' . $name;
       //  // Return it's location
       //  return $filename;
    }
    
}