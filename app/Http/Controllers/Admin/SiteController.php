<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Site;
use App\Category;
use App\Subcategory;
use App\Zoning;
use App\Service;
use App\Contact;

class SiteController extends Controller {

	public $categories;
	public $subcategories;
	public $zoning;
	public $service;

	public function __construct()
	{		
		$this->categories = Category::all();
		$this->subcategories = Subcategory::all();
		$this->zoning = Zoning::all();
		$this->service = Service::all();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{		
		$sites = Site::where('status', '=', 'active')->orderby('address')->get();
		$dormant = Site::where('status', '=', 'dormant')->get();
		return view('admin.site.index', compact('sites','dormant'));
	}


	public function photos()
	{
		$input = \Input::all();
		$file = \Input::file('file'); 
		//$filename = $file->getClientOriginalName();   
		$filename = str_random(12);
		$extension =$file->getClientOriginalExtension(); 
		//$newfilename = $filename . '_' .    $input['site_id'];
        $upload_success = $file->move(public_path() . '/uploads/' . $input['site_id'], $filename . '.' . $extension);
     //    $fields = [
    	// 	'pic' => $filename . '.' . $extension
    	// ];    	
		$site = Site::find($input['site_id']);	
		$site->pic = $filename . '.' . $extension;
		$site->update();		
        if( $upload_success ) {        	      
            return \Response::json('success', 200);
        } else {
            return \Response::json('error', 400);
        }
		//return $input['site_id'];
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = $this->categories;
		$subcategories = $this->subcategories;		
		$zoning_list = Zoning::select(
				'id', 
				\DB::raw('CONCAT(zoning_code, " : ", zoning_description) as zoning'))
			->lists('zoning', 'id');			
		$service_list = Service::select(
				'id',
				\DB::raw('CONCAT(service_code, " : ", service_description) as service'))
			->orderBy('service_code')
			->lists('service', 'id');
		$people_list = Contact::select(
				'id',
				\DB::raw('CONCAT(last_name, ", ", first_name) as full_name'))
			->orderBy('last_name')
			->lists('full_name', 'id');
		return view('admin.site.create', compact( 'categories', 'subcategories', 'zoning_list', 'service_list', 'people_list'));
	 }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$input = \Input::all();
		$site = Site::create($input);
		return redirect('admin/site');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$site = Site::with('contact')->find($id);			
		$categories = $this->categories;
		$subcategories = $this->subcategories;		
		$zoning_list = Zoning::select(
				'id', 
				\DB::raw('CONCAT(zoning_code, " : ", zoning_description) as zoning'))
			->lists('zoning', 'id');			
		$service_list = Service::select(
				'id',
				\DB::raw('CONCAT(service_code, " : ", service_description) as service'))
			->orderBy('service_code')
			->lists('service', 'id');
		$people_list = Contact::select(
				'id',
				\DB::raw('CONCAT(last_name, ", ", first_name) as full_name'))
			->orderBy('last_name')
			->lists('full_name', 'id');

		//$contact = Person::
		// GIVEN SITE
		// FIND ALL CONTACTS
		//
		//dd($site);
		$contact = null;
		if($site->contact) {
			$contact = $site->contact;
		}
		//dd($contact);

		return view('admin.site.show', compact('site', 'categories', 'subcategories', 'zoning_list', 'service_list', 'people_list', 'contact'));
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = \Input::all();
		$site = Site::find($id);
	
		$site->update($input);
		$categories = $this->categories;
		$subcategories = $this->subcategories;		
		$zoning_list = Zoning::select(
				'id', 
				\DB::raw('CONCAT(zoning_code, " : ", zoning_description) as zoning'))
			->lists('zoning', 'id');			
		$service_list = Service::select(
				'id',
				\DB::raw('CONCAT(service_code, " : ", service_description) as service'))
			->orderBy('service_code')
			->lists('service', 'id');
		$people_list = Contact::select(
				'id',
				\DB::raw('CONCAT(last_name, ", ", first_name) as full_name'))
			->orderBy('last_name')
			->lists('full_name', 'id');
		return view('admin.site.show', compact('site', 'categories', 'subcategories', 'zoning_list', 'service_list', 'people_list'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
