<?php namespace App\Http\Controllers;

use App\Site;

class SiteController extends Controller {

	public function display($siteid)
	{
		$site = Site::with('contact')->find($siteid);	
		if(!$site)
		{
			// REDIRECT TO ERROR PAGE WITH FLASH
			dd("NO SITE");
		}

		$url = \Request::server('HTTP_REFERER');	
		//default::centered COBOURG	
		$default_lat = 43.959144;
		$default_lon = -78.167683;
		
		//Get lat/lon from geocoder
		$address = $site->address . ', ' . $site->town . ' ON';
		$geo = \Geocoder::getCoordinatesForQuery($address);
		$map = null;		
		if($geo['lat'] != 0) {	
			$map = \Mapper::map($geo['lat'], $geo['lng'],
			 	['zoom' => 15, 
			 	'center' => true, 
			 	'marker' => true])		
			->render();
		}
		else {
			$map = \Mapper::map($default_lat, $default_lon,
			['zoom' => 15, 
		 	'center' => true, 
		 	'marker' => false])
			->render();
		}

		return view('site.display', compact('site', 'url', 'map'));

	}
}