<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Site;
use App\Category;
use App\Subcategory;

class ReportController extends Controller {
   
    public function printList()
    {      
        $categories = Category::get();
        $subcategories = Subcategory::get();
        $sites = Site::active()->orderby('address')->get();        
        $pdf = \PDF::loadView('pdf.all', compact('sites', 'categories', 'subcategories'))->setPaper('letter', 'landscape');        
        return $pdf->stream();      
    }


    public function printSite($siteid)
    {
        $site = Site::with('contact')->find($siteid);        
        if($site)
        {       
            $pdf = \PDF::loadView('pdf.invoice', compact('site'));          
            return $pdf->stream();
        }
        else
        {
            \Session::flash('error', 'That record does not exist');
            return redirect('/');
        }
    }
    
}