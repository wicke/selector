<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Site;
use App\Filters\SiteFilters;

class SelectorController extends Controller {
			
	public function index(Request $request, SiteFilters $filters)
	{	
		$request->flash();
		$sites = Site::filter($filters)->active()->orderby('address')->get();				
		return view('selector.resultsX', compact('sites'));
	}

}