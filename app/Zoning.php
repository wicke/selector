<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Zoning extends Model {

	protected $table = 'zoning';
	protected $fillable = ['zoning_id', 'zoning_code', 'zoning_description'];

	public function sites()
	{
		return $this->hasMany('App\Site');
	}

}