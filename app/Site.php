<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Filters\QueryFilter;

class Site extends Model {

	protected $table = 'sites';
	protected $fillable = [
		'id',
		'record', 
		'mls', 
		'roll',
		'category_id', 
		'subcategory_id',
		'zoning_id', 
		'service_id', 
		'address',
		'description',
		'legal_description',
		'purchase_price',
		'purchase_price_notes',	
		'lease_price',
		'lease_price_term',
		'lease_price_notes',
		'min_sq_ft',
		'max_sq_ft',
		'total_sq_ft',
		'distance_centers',
		'status',
		'internal_comments',
		'for_sale',
		'for_lease',
		'transaction_type',
		'town',
		'pic'
		];

	public function scopeFilter($query, QueryFilter $filters)
	{
		return $filters->apply($query);
	}

	public function scopeActive($query)
	{
		return $query->where('status', '=', 'active');
	}

	public function scopeRangeFilter($query, $min, $max)
	{			
		$query->where(function($q) use ($min, $max) {
			$q->where('min_sq_ft', '<=', $max)
				->where('max_sq_ft', '>=', $min);
		});		
		return $query;
	}

	public function scopeAcresFilter($query, $min, $max)
	{			
		$query->where(function($q) use ($min, $max) {
			$q->where('acres', '<=', $max)
				->where('acres', '>=', $min);
		});		
		return $query;
	}

	public function scopeForLeaseOrSale($query, $for_sale, $for_lease)
	{		
		if(($for_sale) && (!$for_lease))
		{			
			$query->where('for_sale', '=', 1);		
		}
		elseif (($for_lease) && (!$for_sale))
		{			
			$query->where('for_lease', '=', 1);
		}
		elseif(($for_sale) && ($for_lease))
		{					
			$query->where(function($q){
				$q->where('for_sale', '=', 1)
				->orWhere('for_lease', '=', 1);
			});
		}

		return $query;
		
	}

	public function scopeCategoryFilter($query, $category_id, $subcategory_id )
	{
		if($category_id != 'B' && $subcategory_id == 'B')
		{
			$query->where('category_id', '=', $category_id);			
		}
		if($subcategory_id != 'B' && $category_id == 'B')
		{
			$query->where('subcategory_id', '=', $subcategory_id);			
		}
		if($category_id != 'B' && $subcategory_id != 'B')
		{
			$query->where(function($q) use ($category_id, $subcategory_id) {
				$q->where('category_id', '=', $category_id)
				->where('subcategory_id', '=', $subcategory_id);
			});			
		}
		
		return $query;

	}

	public function scopeSearch($qry, $terms)		
	{
		
	}

	public function scopeCat($query, $category)
	{
		return $query->where('category_id', '=', $category);
	}

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function subcategory()
	{
		return $this->belongsTo('App\Subcategory');
	}

	public function zoning()
	{
		return $this->belongsTo('App\Zoning');
	}

	public function service()
	{
		return $this->belongsTo('App\Service');
	}

	// public function person()
	// {
	// 	return $this->belongsToMany('App\Person', 'person_properties', 'site_id', 'person_id');
	// }

	public function contact()
	{
		//return $this->belongsToMany('App\Contact', 'contact_site', 'site_id', 'contact_id');
		return $this->belongsToMany('App\Contact');
	}

}