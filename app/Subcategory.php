<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model {

	protected $table = 'subcategory';
	protected $fillable = ['subcategory'];

	public function site()
	{
		return $this->hasMany('App\Site');
	}

}