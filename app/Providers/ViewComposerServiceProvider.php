<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Category;
use App\Subcategory;
use App\Range;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->composeSearchBox();	
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	private function composeSearchBox()
	{		
		view()->composer('selector.resultsX', function($view){							
			$view->with('categories', Category::all());			
			$view->with('subcategories', Subcategory::all());		
			$view->with('sqft', Range::sqft()->get());			
			$view->with('acres', Range::acreage()->get());		
		});
	}

}
