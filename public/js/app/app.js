angular
	.module('selectorApp', ['ngResource'], changeSymbol);	

function changeSymbol($interpolateProvider)
{
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');
}