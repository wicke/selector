angular
	.module('selectorApp')
	.controller('SiteCtrl', SiteController);

function SiteController(siteFactory, $scope)
{
	var vm = this;
	vm.sites = [];

	siteFactory.get()
		.then(function(data){
			vm.sites = data;
		});

	$scope.sort = function(keyname) {
		
		$scope.sortKey = keyname;
		$scope.reverse = !$scope.reverse;
	}
}