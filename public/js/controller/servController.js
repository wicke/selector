angular
	.module('selectorApp')
	.controller('ServiceCtrl', ServController);

function ServController(serviceFactory, $scope)
{
	var vm = this;
	vm.services = [];

	serviceFactory.get()
		.then(function(data){
			vm.services = data;
		});

	
	$scope.remove = function(id) {
		serviceFactory.delete(id).then(function(success){
			getClients();			
		}, function(error) {
			console.log(error);
		})
	}




}