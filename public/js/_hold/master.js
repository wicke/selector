$(document).ready(function($) {

	

	function responsiveView() {
		var wSize = $(window).width();
		if(wSize <=768) {
			$('.wrapper').addClass('sidebar-closed');
			$('.sidenav > ul').hide();			 
		}

		if(wSize > 768) {
			$('.wrapper').removeClass('sidebar-closed');
			$('.sidenav > ul').show();			
		}

	}

	$(window).on('load', responsiveView);
	$(window).on('resize', responsiveView);






	$('.burger').click(function() {
		if($('.sidenav > ul').is(":visible") === true) {
			$('#main-container').css({ 
				'margin-left' : '0px'
			});
			$('.sidenav').css({
				'margin-left' : '-210px'
			});
			$('.sidenav > ul').hide();
			 $(".wrapper").addClass("sidebar-closed");
		} else {
			$('#main-container').css({ 
				'margin-left' : '210px'
			});
			$('.sidenav > ul').show();		
			$('.sidenav').css({
				'margin-left' : '0px'
			});
				
			$(".wrapper").removeClass("sidebar-closed");
		}
	});
	
});


