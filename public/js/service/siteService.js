angular
	.module('selectorApp')
	.factory('siteFactory', siteService);

function siteService($resource)
{
	var Site = $resource('/api/site');

	return {
		get : getSites
	}


	function getSites()
	{
		return Site.query().$promise
			.then(function(results){
				return results;
			});
	}

	
}