angular
	.module('selectorApp')
	.factory('serviceFactory', servService);

function servService($resource)
{
	var Service = $resource('/api/service');

	return {
		get : getServices,
		delete: deleteService
	}


	function getServices()
	{
		return Service.query().$promise
			.then(function(results){
				return results;
			});
	}

	function deleteClient(id) {
		return Service.delete({id:id}).$promise.then(function(success) {
			console.log(success);
		}, function(error) {
			console.log(error);			
		});
	}
	
}