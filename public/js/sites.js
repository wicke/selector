var SearchComponent = Vue.extend({
    template: '#site-search',
    methods: {
        filterList: function() {

        }
    }
});

var MyComponent = Vue.extend({
    template: '#site-table',

    data: function() {

        return {
            //for_sale: 'B',
            
            pricing: '{pricing:4}',  
            category: '{cat:3}',        
            list: []
        };
    },

    ready: function() {
        this.fetchSiteList();
    },
    
    // created: function() {
    //     this.fetchSiteList();
    // },


    filters: {
        pricing: function(site) {
            var self = this; 
             if(self.pricing == '4') {
                 return site;
             }
            return site.filter(function(sale){
                return sale.pricing == self.pricing;
            });
        },

        test: function(site) {
            var self = this; 
            var pricing = self.pricing;
            var category = self.category;
           // return site.filter(function(price, cat){
           //      price.pricing = self.price;
           //      cat.category = self.cat;
           //      return site;
           // });
            
        }
        
    },


    methods: {
        fetchSiteList: function() {
            var vm = this;            
            var resource = vm.$resource('/api/site/:id');
             //vm.availablity: 'B';
            resource.get(function(sites){
                vm.list = sites;

            });
        },
        filterList: function(id) {
            var vm = this;           
            var resource = vm.$resource('/api/site/filter/:id');          
            resource.get( {id : id}, function(sites){
                vm.list = sites;
            });
        },
        filterCategory: function(category, pricing) {
            var vm = this;            
            var resource = vm.$resource('/api/site/filter/:id');
            console.log(category);
            console.log(pricing);
            resource.get( {cat : category.cat, pricing : pricing.pricing}, function(sites){
                vm.list = sites;
            })
        }
    }
});



Vue.component('site-list', MyComponent);


new Vue({

    el: "#app"

});