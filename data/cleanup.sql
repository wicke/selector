-- Dump completed on 2017-02-06  1:01:56
update sites
set for_sale = 1
where (purchase_price is not null and purchase_price <> '')
or (purchase_price_notes is not null and purchase_price_notes <> '')
or lcase(description) like "%sale%";

update sites
set for_lease = 1
where (lease_price is not null and lease_price <> '')
or (lease_price_notes is not null and lease_price_notes <> '')
or lcase(description) like "%lease%";

update sites
set for_lease = 1
where for_lease = 0 and for_sale = 0;


DELETE FROM `sites` WHERE status <> 'active';