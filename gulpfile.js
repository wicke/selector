var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('masterB.scss', 'public/css/masterB.css');
    mix.sass('report.scss', 'public/css/report.css');
    mix.browserSync({ 
        proxy: 'selector.app',
        browser: 'google chrome',
        files: ['resources/views/**/*.php']
    });
});